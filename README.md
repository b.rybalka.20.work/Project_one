Build:
Выполнять в папке с Dockerfile.
sudo docker build -t rally .

Start:
sudo docker run rally -year 2017 -apiKeys apiKeys 
-connectionTimeout 1000 -statisticsTable statTable 
-identityTable identityTable -dbBase base -dbServer server -dbUsername username
-dbPassword password
