package com.nixsolutions.statisticsapp.rally;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.nixsolutions.statisticsapp.aspect.annotation.LoopWhileException;
import com.nixsolutions.statisticsapp.utils.RallyEntitiesUtils;
import com.rallydev.rest.RallyRestApi;
import com.rallydev.rest.request.QueryRequest;
import com.rallydev.rest.response.QueryResponse;
import com.rallydev.rest.util.Fetch;
import com.rallydev.rest.util.QueryFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.*;

import static com.nixsolutions.statisticsapp.utils.DateUtils.getIsoDateOfFirstDayInYear;
import static com.nixsolutions.statisticsapp.utils.DateUtils.getIsoDateOfLastDayInYear;
import static com.nixsolutions.statisticsapp.utils.QueryFilterUtils.createFilterCompletedOrReadyToTest;
import static com.nixsolutions.statisticsapp.utils.QueryFilterUtils.createFilterInProgressDateBetween;

@Component
public class Defects {

    private String developerAccount;
    private RestApiFactory restApiFactory;
    private RallyRestApi restApi;

    @Autowired
    public Defects(RestApiFactory restApiFactory) {
        this.restApiFactory = restApiFactory;
    }

    public String getDeveloperAccount() {
        return developerAccount;
    }

    public void setDeveloperAccount(String developerAccount) {
        this.developerAccount = developerAccount;
    }

    @LoopWhileException
    public Map<Integer, JsonArray> getAllDefectsByMonths(int year) throws Exception {
        try {
            restApi = restApiFactory.getRestApi();
            Map<Integer, JsonArray> defectsByMonths = new HashMap<>();

            String before = getIsoDateOfLastDayInYear(year);
            String after = getIsoDateOfFirstDayInYear(year);

            QueryRequest defectsQueryRequest = new QueryRequest("defects");
            defectsQueryRequest.setQueryFilter(createFilterInProgressDateBetween(before, after));
            defectsQueryRequest.setFetch(new Fetch("RevisionHistory", "Revisions", "InProgressDate",
                    "FormattedID", "PlanEstimate"));

            QueryResponse query = restApi.query(defectsQueryRequest);
            return RallyEntitiesUtils.sortRallyEntitiesWithInProgressDateByMonths(query.getResults());
        } finally {
            restApi.close();
        }
    }
    
    @LoopWhileException
    public List<JsonObject> getCompletedUsersDefects(JsonArray defects) throws URISyntaxException, IOException {
        try {
            restApi = restApiFactory.getRestApi();
            List<JsonObject> completedDefects = new ArrayList<>();
            for (int i = 0; i < defects.size(); i++) {
                JsonObject defect = defects.get(i).getAsJsonObject();
                QueryRequest queryRequest = new QueryRequest(defect.get("RevisionHistory").getAsJsonObject().get("Revisions").getAsJsonObject());
                queryRequest.setQueryFilter(new QueryFilter("User.UserName", "=", developerAccount.toLowerCase())
                        .and(createFilterCompletedOrReadyToTest()));

                QueryResponse query = restApi.query(queryRequest);
                if (query.getResults().size() > 0) {
                    completedDefects.add(defect);
                    defects.remove(defect);
                    i--;
                }
            }
            return completedDefects;
        } finally {
            restApi.close();
        }
    }

}
