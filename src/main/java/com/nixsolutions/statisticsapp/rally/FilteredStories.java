package com.nixsolutions.statisticsapp.rally;

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.*;

import com.google.gson.JsonArray;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.nixsolutions.statisticsapp.aspect.annotation.LoopWhileException;
import com.nixsolutions.statisticsapp.utils.RallyEntitiesUtils;
import com.rallydev.rest.RallyRestApi;
import com.rallydev.rest.request.QueryRequest;
import com.rallydev.rest.response.QueryResponse;
import com.rallydev.rest.util.Fetch;
import com.rallydev.rest.util.QueryFilter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.nixsolutions.statisticsapp.utils.QueryFilterUtils.createFilterCompletedOrReadyToTest;
import static com.nixsolutions.statisticsapp.utils.QueryFilterUtils.createFilterHasTasks;
import static com.nixsolutions.statisticsapp.utils.QueryFilterUtils.createFilterInProgressDateBetween;
import static com.nixsolutions.statisticsapp.utils.DateUtils.getIsoDateOfFirstDayInYear;
import static com.nixsolutions.statisticsapp.utils.DateUtils.getIsoDateOfLastDayInYear;

@Component
public class FilteredStories {
    private String developerAccount;
    private RestApiFactory restApiFactory;
    private RallyRestApi restApi;

    @Autowired
    public FilteredStories(RestApiFactory restApiFactory) {
        this.restApiFactory = restApiFactory;
    }

    public String getDeveloperAccount() {
        return developerAccount;
    }

    public void setDeveloperAccount(String developerAccount) throws URISyntaxException {
        this.developerAccount = developerAccount;
    }
    
    @LoopWhileException
    public Map<Integer, JsonArray> getAllStoriesWithTasksSortedByMonths(int year) throws URISyntaxException, IOException, ParseException {
        try {
            restApi = restApiFactory.getRestApi();
            Map<Integer, JsonArray> storiesWithTasksByMonths = new HashMap<>();

            String before = getIsoDateOfLastDayInYear(year);
            String after = getIsoDateOfFirstDayInYear(year);

            QueryRequest queryRequest = new QueryRequest("hierarchicalrequirement");
            queryRequest.setQueryFilter(createFilterHasTasks()
                    .and(createFilterInProgressDateBetween(before, after)));
            queryRequest.setFetch(new Fetch("FormattedID", "Name", "Tasks", "PlanEstimate", "InProgressDate"));

            JsonArray allStoriesWithTasks = restApi.query(queryRequest).getResults();
            return RallyEntitiesUtils.sortRallyEntitiesWithInProgressDateByMonths(allStoriesWithTasks);
        } finally {
            restApi.close();
        }
    }
    
    @LoopWhileException
    public Map<Integer, JsonArray> getAllStoriesWithRevisionsSortedByMonths(int year) throws IOException, ParseException, URISyntaxException {
        try {
            restApi = restApiFactory.getRestApi();
            Map<Integer, JsonArray> completedStoriesByMonths = new HashMap<>();

            String before = getIsoDateOfLastDayInYear(year);
            String after = getIsoDateOfFirstDayInYear(year);

            QueryRequest queryRequest = new QueryRequest("hierarchicalrequirement");
            queryRequest.setQueryFilter(new QueryFilter("PlanEstimate", ">", "0")
                    .and(createFilterInProgressDateBetween(before, after)));
            queryRequest.setFetch(new Fetch("FormattedID", "Name", "Owner", "PlanEstimate", "InProgressDate", "RevisionHistory", "Revisions"));

            JsonArray allCompletedStories = restApi.query(queryRequest).getResults();
            return RallyEntitiesUtils.sortRallyEntitiesWithInProgressDateByMonths(allCompletedStories);
        } finally {
            restApi.close();
        }
    }
    
    @LoopWhileException
    public List<JsonObject> getUsersDesignReviewStories(JsonArray stories) throws IOException, URISyntaxException {
        try {
            List<JsonObject> storiesOfDesignReview = new ArrayList<>();
            restApi = restApiFactory.getRestApi();
            for (int i = 0; i < stories.size(); i++) {
                JsonElement jsonElement = stories.get(i);

                JsonObject tasks = jsonElement.getAsJsonObject().getAsJsonObject("Tasks");
                QueryRequest taskRequest = new QueryRequest(tasks);
                taskRequest.setFetch(new Fetch("Name"));
                taskRequest.setQueryFilter(new QueryFilter("Owner.UserName", "=", developerAccount.toLowerCase()));

                QueryResponse queryResponse = restApi.query(taskRequest);

                JsonArray tasksJsonArray = queryResponse.getResults();
                for (JsonElement element : tasksJsonArray) {
                    JsonObject jsonObject = element.getAsJsonObject();
                    if (StringUtils.equalsIgnoreCase(jsonObject.get("Name").getAsString(), "Design Review")) {
                        storiesOfDesignReview.add(jsonElement.getAsJsonObject());
                        stories.remove(jsonElement.getAsJsonObject());
                        i--;
                        break;
                    }
                }
            }
            return storiesOfDesignReview;
        } finally {
            restApi.close();
        }
    }
    
    
    @LoopWhileException
    public List<JsonObject> getCompletedUsersStories(JsonArray stories) throws IOException, URISyntaxException {
        try {
            restApi = restApiFactory.getRestApi();

            List<JsonObject> completedStories = new ArrayList<>();
            for (int i = 0; i < stories.size(); i++) {
                JsonObject story = stories.get(i).getAsJsonObject();

                QueryRequest queryRequest = new QueryRequest(story.get("RevisionHistory").getAsJsonObject().get("Revisions").getAsJsonObject());
                queryRequest.setFetch(new Fetch("FormattedID", "Name", "Owner", "PlanEstimate", "InProgressDate", "RevisionHistory", "Revisions"));
                queryRequest.setQueryFilter(new QueryFilter("User.UserName", "=", developerAccount.toLowerCase())
                        .and(createFilterCompletedOrReadyToTest()));

                QueryResponse query = restApi.query(queryRequest);
                if (query.getResults().size() > 0) {
                    completedStories.add(story);
                    stories.remove(story);
                    i--;
                }
            }
            return completedStories;
        } finally {
            restApi.close();
        }
    }
}
