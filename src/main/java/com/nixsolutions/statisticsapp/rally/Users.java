package com.nixsolutions.statisticsapp.rally;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Optional;

import com.nixsolutions.statisticsapp.aspect.annotation.LoopWhileException;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.JsonElement;
import com.rallydev.rest.RallyRestApi;
import com.rallydev.rest.request.QueryRequest;
import com.rallydev.rest.response.QueryResponse;
import com.rallydev.rest.util.QueryFilter;
import org.springframework.stereotype.Component;

@Component
public class Users {

    private RestApiFactory restApiFactory;

    @Autowired
    public Users(RestApiFactory restApiFactory) {
        this.restApiFactory = restApiFactory;
    }

    private QueryFilter createFilterToFindUserByUserName(String ... userNames ) {
        if (userNames.length == 0) {
            return null;
        }

        QueryFilter queryFilter = new QueryFilter("UserName", "=", userNames[0].toLowerCase());
        for (int i = 1; i < userNames.length; i++) {
            queryFilter = queryFilter.or(new QueryFilter("UserName", "=", userNames[i].toLowerCase()));
        }
        return queryFilter;
    }
    
    @LoopWhileException
    public JsonElement getUserByBatchEmailAddress(String[] emails) throws URISyntaxException, IOException {

        try (RallyRestApi restApi = restApiFactory.getRestApi() ) {

            QueryRequest queryRequest = new QueryRequest("user");
            QueryFilter queryFilter = createFilterToFindUserByUserName(emails);
            queryRequest.setQueryFilter(queryFilter);
            QueryResponse query = restApi.query(queryRequest);

            return query.getResults().size() == 0 ? null : query.getResults().get(0);
        }
    }

}
