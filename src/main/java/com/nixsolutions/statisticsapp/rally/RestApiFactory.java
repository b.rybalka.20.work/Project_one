package com.nixsolutions.statisticsapp.rally;

import java.net.URI;
import java.net.URISyntaxException;

import com.rallydev.rest.RallyRestApi;

public class RestApiFactory {

    //Specify your Rally server
    private final String SERVER = "https://rally1.rallydev.com";

    //Specify your WSAPI version
    private final String WSAPI_VERSION = "v2.0";

    //Specify your Rally username
    private String USERNAME = "";

    //Specify your Rally password
    private String PASSWORD = "";

    //Specify your Rally api key
    private String API_KEY = "";

    //If using a proxy specify full url, like http://my.proxy.com:8000
    private String PROXY_SERVER = null;

    //If using an authenticated proxy server specify the username and password
    private String PROXY_USERNAME = null;
    private String PROXY_PASSWORD = null;

    public RestApiFactory() {
    }

    public RallyRestApi getRestApi() throws URISyntaxException {
        RallyRestApi restApi;

        if(API_KEY != null && !API_KEY.equals("")) {
            restApi = new RallyRestApi(new URI(SERVER), API_KEY);
        } else {
            restApi = new RallyRestApi(new URI(SERVER), USERNAME, PASSWORD);
        }
        if (PROXY_SERVER != null) {
            URI uri = new URI(PROXY_SERVER);
            if (PROXY_USERNAME != null) {
                restApi.setProxy(uri, PROXY_USERNAME, PROXY_PASSWORD);
            } else {
                restApi.setProxy(uri);
            }
        }
        restApi.setWsapiVersion(WSAPI_VERSION);

        return restApi;
    }

    public String getAPI_KEY () {
        return API_KEY;
    }

    public void setAPI_KEY(String apiKey) {
        API_KEY = apiKey;
    }
}
