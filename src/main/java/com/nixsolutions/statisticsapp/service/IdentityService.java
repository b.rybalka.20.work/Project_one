package com.nixsolutions.statisticsapp.service;

import com.nixsolutions.statisticsapp.domain.Identity;
import com.nixsolutions.statisticsapp.repository.IdentityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class IdentityService {
    private IdentityRepository identityRepository;

    @Autowired
    public IdentityService(IdentityRepository identityRepository) {
        this.identityRepository = identityRepository;
    }

    public List<Identity> findAllIdentitiesWhereRallyNameExists() {
        return identityRepository.findAllWhereRallyNameExists();
    }
    public Optional<Identity> save(Identity identity) {
        return identityRepository.save(identity);
    }
}
