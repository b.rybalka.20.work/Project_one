package com.nixsolutions.statisticsapp.service;

import com.nixsolutions.statisticsapp.utils.DateUtils;
import com.nixsolutions.statisticsapp.domain.Identity;
import com.nixsolutions.statisticsapp.domain.Statistics;
import com.nixsolutions.statisticsapp.repository.StatisticsRepository;
import org.apache.commons.collections4.ListUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

@Service
public class StatisticsService {
    private StatisticsRepository statisticsRepository;

    @Autowired
    public StatisticsService(StatisticsRepository statisticsRepository) {
        this.statisticsRepository = statisticsRepository;
    }

    public Optional<Statistics> saveStatistics(Identity identity, List<Statistics.Ticket> defects, List<Statistics.Ticket> stories) throws ParseException {
        List<Statistics.Ticket> tickets = ListUtils.sum(defects, stories);
        Statistics.Ticket ticket = tickets.get(0);
        if (ticket == null)
            return Optional.empty();

        Calendar inProgressDate = DateUtils.getCalendarByISOdateTime(ticket.getInProgressDate());

        Statistics.StatisticsId statisticsId = new Statistics.StatisticsId();
        statisticsId.setEmployee(identity.getEmployee());
        statisticsId.setMonth(inProgressDate.get(Calendar.MONTH) + 1);
        statisticsId.setYear(inProgressDate.get(Calendar.YEAR));

        List<Statistics.Ticket> estimatedTickets = new ArrayList<>();
        List<Statistics.Ticket> unestimatedTickets = new ArrayList<>();
        tickets.forEach(t -> {
           if (t.getPlanEstimate() == 0.0D) {
               unestimatedTickets.add(t);
           } else {
               estimatedTickets.add(t);
           }
        } );

        Statistics statistics = new Statistics();
        statistics.setId(statisticsId);
        statistics.setEstimatedTickets(estimatedTickets);
        statistics.setUnestimatedTickets(unestimatedTickets);
        statistics.setTotal(estimatedTickets.size());
        statistics.setStoryPoints(estimatedTickets
                .stream()
                .mapToDouble(Statistics.Ticket::getPlanEstimate)
                .sum());
        return statisticsRepository.save(statistics);
    }
}
