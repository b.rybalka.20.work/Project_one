package com.nixsolutions.statisticsapp;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Properties;

public class Options {
    private String[] apiKeys;
    private String statisticsTable;
    private String identityTable;
    private String dbBase;
    private String dbServer;
    private Integer year;
    private Integer connectionTimeout;
    private String username;
    private String password;

    private Properties properties;

    private static final Integer DEFAULT_CONNECTION_TIMEOUT = 1000;

    public Options() {
        properties = new Properties();
        String propertiesLocation = System.getProperty("properties");
        if (propertiesLocation != null) {
            try {
                properties.load(new FileInputStream(propertiesLocation));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        apiKeys = Arrays.stream(getProperty("apiKeys").split(" "))
                .filter(key -> !key.equals(""))
                .toArray(String[]::new);
        year = Integer.valueOf(getProperty("year"));
        connectionTimeout = getProperty("connectionTimeout") == null
                ? DEFAULT_CONNECTION_TIMEOUT
                : Integer.valueOf(getProperty("connectionTimeout"));
        statisticsTable = getProperty("statisticsTable");
        identityTable = getProperty("identityTable");
        dbBase = getProperty("dbBase");
        dbServer = getProperty("dbServer");
        username = getProperty("dbUsername");
        password = getProperty("dbPassword");
    }

    public String[] getApiKeys() {
        return apiKeys;
    }

    public void setApiKeys(String[] apiKeys) {
        this.apiKeys = apiKeys;
    }

    public String getStatisticsTable() {
        return statisticsTable;
    }

    public void setStatisticsTable(String statisticsTable) {
        this.statisticsTable = statisticsTable;
    }

    public String getIdentityTable() {
        return identityTable;
    }

    public void setIdentityTable(String identityTable) {
        this.identityTable = identityTable;
    }

    public String getDbBase() {
        return dbBase;
    }

    public void setDbBase(String dbBase) {
        this.dbBase = dbBase;
    }

    public String getDbServer() {
        return dbServer;
    }

    public void setDbServer(String dbServer) {
        this.dbServer = dbServer;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getConnectionTimeout() {
        return connectionTimeout;
    }

    public void setConnectionTimeout(Integer connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    private String getProperty(String key) {
        return System.getProperty(key) == null ?
                properties.getProperty(key): System.getProperty(key);
    }
}
