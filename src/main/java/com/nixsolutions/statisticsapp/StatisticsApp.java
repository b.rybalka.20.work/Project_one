package com.nixsolutions.statisticsapp;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.nixsolutions.statisticsapp.domain.Identity;
import com.nixsolutions.statisticsapp.domain.Statistics;
import com.nixsolutions.statisticsapp.rally.Defects;
import com.nixsolutions.statisticsapp.rally.FilteredStories;
import com.nixsolutions.statisticsapp.rally.RestApiFactory;
import com.nixsolutions.statisticsapp.rally.Users;
import com.nixsolutions.statisticsapp.service.IdentityService;
import com.nixsolutions.statisticsapp.service.StatisticsService;
import com.nixsolutions.statisticsapp.utils.RallyEntitiesUtils;
import org.apache.commons.collections4.ListUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class StatisticsApp implements CommandLineRunner {
    private IdentityService identityService;
    private StatisticsService statisticsService;
    private Options options;
    private Users users;
    private FilteredStories filteredStories;
    private Defects defects;
    private RestApiFactory restApiFactory;
    
    @Autowired
    public StatisticsApp(IdentityService identityService, StatisticsService statisticsService, Options options, Users users, FilteredStories filteredStories, Defects defects, RestApiFactory restApiFactory) {
        this.identityService = identityService;
        this.statisticsService = statisticsService;
        this.options = options;
        this.users = users;
        this.filteredStories = filteredStories;
        this.defects = defects;
        this.restApiFactory = restApiFactory;
    }

    @Override
    public void run(String... args) throws Exception {
        List<Identity> identityList = identityService.findAllIdentitiesWhereRallyNameExists();
        for (String apiKey : options.getApiKeys()) {
            restApiFactory.setAPI_KEY(apiKey);
            Map<Integer, JsonArray> allDefectsByMonths = defects.getAllDefectsByMonths(options.getYear());
            Map<Integer, JsonArray> allStoriesWithTasksByMonths = filteredStories.getAllStoriesWithTasksSortedByMonths(options.getYear());
            Map<Integer, JsonArray> allStoriesWithRevisionsByMonth = filteredStories.getAllStoriesWithRevisionsSortedByMonths(options.getYear());

            for (Identity userToProcess : identityList) {
                String emails = userToProcess.getParexelGitlab() == null ?
                        userToProcess.getIbmGitlab() : userToProcess.getParexelGitlab();

                JsonElement rallyUser = users.getUserByBatchEmailAddress(splitEmails(emails));
                String userName = rallyUser.getAsJsonObject().get("UserName").getAsString();
                defects.setDeveloperAccount(userName);
                filteredStories.setDeveloperAccount(userName);
                for (int month = 0; month < 12 ; month++) {

                    List<JsonObject> defects1 = defects.getCompletedUsersDefects(allDefectsByMonths.get(month));
                    List<JsonObject> resultUS1 = filteredStories.getUsersDesignReviewStories(allStoriesWithTasksByMonths.get(month));
                    List<JsonObject> resultUS2 = filteredStories.getCompletedUsersStories(allStoriesWithRevisionsByMonth.get(month));

                    List<JsonObject> resultUS = new ArrayList<>(ListUtils.sum(resultUS1, resultUS2).stream()
                            .collect(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(p -> p.get("FormattedID").getAsString())))));

                    List<Statistics.Ticket> defects = RallyEntitiesUtils.toDefects(defects1);
                    List<Statistics.Ticket> stories = RallyEntitiesUtils.toStories(resultUS);

                    statisticsService.saveStatistics(userToProcess, defects, stories);
                }
            }
        }
    }

    private String[] splitEmails(String emails) {
        return Arrays.stream(emails.split(" "))
                .filter(email -> !email.equals(""))
                .map(String::trim)
                .toArray(String[]::new);
    }
}
