package com.nixsolutions.statisticsapp.repository;

import com.nixsolutions.statisticsapp.domain.Identity;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.Optional;

public interface IdentityRepository extends Repository<Identity, String> {
    @Query(value = "{rallyName: {$exists: true}}")
    List<Identity> findAllWhereRallyNameExists();
    Optional<Identity> save(Identity identity);
}
