package com.nixsolutions.statisticsapp.repository;

import com.nixsolutions.statisticsapp.domain.Statistics;
import org.springframework.data.repository.Repository;

import java.util.Optional;

public interface StatisticsRepository extends Repository<Statistics, String> {
    Optional<Statistics> save(Statistics statistics);
}
