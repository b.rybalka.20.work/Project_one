package com.nixsolutions.statisticsapp.utils;

import com.rallydev.rest.util.QueryFilter;

public class QueryFilterUtils {
    public static QueryFilter createFilterCompletedOrReadyToTest() {
        return new QueryFilter("Description", "contains", "[Dev In-Progress] to [Ready for Test]")
                .or(new QueryFilter("Description", "contains", "] to [Completed]"));
    }

    public static QueryFilter createFilterInProgressDateBetween(String before, String after) {
        return new QueryFilter("InProgressDate", "<=", before)
                .and(new QueryFilter("InProgressDate", ">=", after));
    }

    public static QueryFilter createFilterHasTasks () {
        return new QueryFilter("Tasks.ObjectID", "!=", null);
    }
}
