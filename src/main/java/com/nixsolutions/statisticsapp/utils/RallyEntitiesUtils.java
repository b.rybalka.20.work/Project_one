package com.nixsolutions.statisticsapp.utils;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.nixsolutions.statisticsapp.domain.Statistics;

import java.text.ParseException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.nixsolutions.statisticsapp.utils.DateUtils.getCalendarByISOdateTime;

public class RallyEntitiesUtils {
    public static Map<Integer, JsonArray> sortRallyEntitiesWithInProgressDateByMonths(JsonArray elements) throws ParseException {
        Map<Integer, JsonArray> elementsByMonths = new HashMap<>();
        for (JsonElement jsonElement : elements) {
            JsonObject elementJsonObject = jsonElement.getAsJsonObject();
            String inProgressDateStr = elementJsonObject.get("InProgressDate")
                    .toString().replace("\"", "");
            Calendar inProgressDateCalendar = getCalendarByISOdateTime(inProgressDateStr);
            int month = inProgressDateCalendar.get(Calendar.MONTH);
            if (elementsByMonths.keySet().contains(month)) {
                elementsByMonths.get(month).add(elementJsonObject);
            } else {
                JsonArray completedStoriesInMonth = new JsonArray();
                completedStoriesInMonth.add(elementJsonObject);
                elementsByMonths.put(month, completedStoriesInMonth);
            }
        }
        return elementsByMonths;
    }

    public static Statistics.Ticket getDefect(JsonObject jsonObject) {
        HashMap<String, Object> report = new Gson().fromJson(jsonObject, HashMap.class);

        Statistics.TicketImpl defect = new Statistics.TicketImpl();
        defect.setFormattedId((String) report.get("FormattedID"));
        defect.setInProgressDate((String) report.get("InProgressDate"));

        Object planEstimate = report.get("PlanEstimate");
        if (planEstimate == null || ((Number) planEstimate).doubleValue() == 0D) {
            defect.setPlanEstimate(0D);
        } else {
            defect.setPlanEstimate(new Double(planEstimate.toString()));
        }
        return defect;
    }

    public static List<Statistics.Ticket> toDefects(List<JsonObject> jsonObjects) {
        return jsonObjects.stream()
                .map(RallyEntitiesUtils::getDefect)
                .collect(Collectors.toList());
    }

    public static Statistics.Ticket getStory(JsonObject jsonObject) {
        HashMap<String, Object> report = new Gson().fromJson(jsonObject, HashMap.class);

        Statistics.TicketImpl story = new Statistics.TicketImpl();

        story.setFormattedId((String) report.get("FormattedID"));
        story.setFullName((String) report.get("Name"));
        story.setInProgressDate((String) report.get("InProgressDate"));
        story.setFullName((String) report.get("FullName"));
        story.setPlanEstimate((Double) report.get("PlanEstimate"));

        return story;
    }

    public static List<Statistics.Ticket> toStories(List<JsonObject> jsonObjects) {
        return jsonObjects.stream()
                .map(RallyEntitiesUtils::getStory)
                .collect(Collectors.toList());
    }
}
