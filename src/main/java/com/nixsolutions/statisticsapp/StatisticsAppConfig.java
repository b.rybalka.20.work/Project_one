package com.nixsolutions.statisticsapp;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.nixsolutions.statisticsapp.rally.RestApiFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import java.util.Collections;

@SpringBootApplication
@EnableMongoRepositories
@EnableAspectJAutoProxy
public class StatisticsAppConfig extends AbstractMongoConfiguration {

    public @Bean("options") Options Options() {
        return new Options();
    }
    
    public @Bean RestApiFactory restApiFactory() {
        return new RestApiFactory();
    }

    @Override
    protected String getDatabaseName() {
        return Options().getDbBase();
    }

    @Override
    public Mongo mongo() throws Exception {
        String dbServer = Options().getDbServer();
        String username = Options().getUsername();
        String password = Options().getPassword();
        String dbBase = Options().getDbBase();

        if (username == null && password == null)
            return new MongoClient(dbServer);

        return new MongoClient(new ServerAddress(dbServer),
                Collections.singletonList(MongoCredential.createCredential(username, dbBase,password.toCharArray())) );
    }

    @Override
    public MappingMongoConverter mappingMongoConverter() throws Exception {
        MappingMongoConverter mappingMongoConverter = super.mappingMongoConverter();
        mappingMongoConverter.setTypeMapper(new DefaultMongoTypeMapper(null));
        return mappingMongoConverter;
    }

    public static void main(String[] args) {
        SpringApplication.run(StatisticsAppConfig.class, args);
    }
}
