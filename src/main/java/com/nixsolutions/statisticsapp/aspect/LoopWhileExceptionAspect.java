package com.nixsolutions.statisticsapp.aspect;


import com.nixsolutions.statisticsapp.Options;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class LoopWhileExceptionAspect {
    
    public Options options;
    
    @Autowired
    public LoopWhileExceptionAspect(Options options) {
        this.options = options;
    }
    
    @Around("@annotation(com.nixsolutions.statisticsapp.aspect.annotation.LoopWhileException)")
    public Object aroundSampleCreation(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        while (true) {
            try {
                return proceedingJoinPoint.proceed();
            } catch (Exception e) {
                e.printStackTrace();
                Thread.sleep(options.getConnectionTimeout());
            }
        }
    }
}
