package com.nixsolutions.statisticsapp.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.List;

@Document(collection = "#{@options.getStatisticsTable()}")
public class Statistics implements Serializable {
    @Id
    private StatisticsId id;

    private List<Ticket> estimatedTickets;
    private List<Ticket> unestimatedTickets;
    private Integer total;
    private Double storyPoints;
    private Double timeSpent = 0.0D;
    private Integer reopensNumber = 0;
    private Integer intrasprintsNumber = 0;

    public static class StatisticsId implements Serializable {
        private String employee;
        private Integer year;
        private Integer month;

        public String getEmployee() {
            return employee;
        }

        public void setEmployee(String employee) {
            this.employee = employee;
        }

        public Integer getYear() {
            return year;
        }

        public void setYear(Integer year) {
            this.year = year;
        }

        public Integer getMonth() {
            return month;
        }

        public void setMonth(Integer month) {
            this.month = month;
        }
    }

    public StatisticsId getId() {
        return id;
    }

    public void setId(StatisticsId id) {
        this.id = id;
    }

    public List<Ticket> getEstimatedTickets() {
        return estimatedTickets;
    }

    public void setEstimatedTickets(List<Ticket> estimatedTickets) {
        this.estimatedTickets = estimatedTickets;
    }

    public List<Ticket> getUnestimatedTickets() {
        return unestimatedTickets;
    }

    public void setUnestimatedTickets(List<Ticket> unestimatedTickets) {
        this.unestimatedTickets = unestimatedTickets;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Double getStoryPoints() {
        return storyPoints;
    }

    public void setStoryPoints(Double storyPoints) {
        this.storyPoints = storyPoints;
    }

    public Double getTimeSpent() {
        return timeSpent;
    }

    public void setTimeSpent(Double timeSpent) {
        this.timeSpent = timeSpent;
    }

    public Integer getReopensNumber() {
        return reopensNumber;
    }

    public void setReopensNumber(Integer reopensNumber) {
        this.reopensNumber = reopensNumber;
    }

    public Integer getIntrasprintsNumber() {
        return intrasprintsNumber;
    }

    public void setIntrasprintsNumber(Integer intrasprintsNumber) {
        this.intrasprintsNumber = intrasprintsNumber;
    }

    public interface Ticket extends Serializable {
        String getInProgressDate();
        String getFormattedId();
        String getFullName();
        Double getPlanEstimate();
        void setInProgressDate(String str);
        void setFormattedId(String str);
        void setFullName(String str);
        void setPlanEstimate(Double planEstimate);
    }

    public static class TicketImpl implements Ticket {

        private String inProgressDate;
        private String formattedId;
        private String fullName;
        private Double planEstimate;

        @Override
        public String getInProgressDate() {
            return inProgressDate;
        }

        @Override
        public String getFormattedId() {
            return formattedId;
        }

        @Override
        public String getFullName() {
            return fullName;
        }

        @Override
        public Double getPlanEstimate() {
            return planEstimate;
        }

        @Override
        public void setInProgressDate(String str) {
            this.inProgressDate = str;
        }

        @Override
        public void setFormattedId(String str) {
            this.formattedId = str;
        }

        @Override
        public void setFullName(String str) {
            this.fullName = str;
        }

        @Override
        public void setPlanEstimate(Double planEstimate) {
            this.planEstimate = planEstimate;
        }
    }
}
