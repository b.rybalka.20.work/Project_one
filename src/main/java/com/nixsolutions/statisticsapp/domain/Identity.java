package com.nixsolutions.statisticsapp.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;

@Document(collection = "#{@options.getIdentityTable()}")
public class Identity implements Serializable {
    @Id
    private String id;
    private Integer leadId;
    private Boolean active;
    private String email;
    private String employee;
    private String from;
    private String login;
    @Field(value = "movement_employee")
    private String movementEmployee;
    @Field(value = "parexel_gitlab")
    private String parexelGitlab;
    @Field(value = "ibm_gitlab")
    private String ibmGitlab;
    private String rallyName;
    private String updated;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getLeadId() {
        return leadId;
    }

    public void setLeadId(Integer leadId) {
        this.leadId = leadId;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmployee() {
        return employee;
    }

    public void setEmployee(String employee) {
        this.employee = employee;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMovementEmployee() {
        return movementEmployee;
    }

    public void setMovementEmployee(String movementEmployee) {
        this.movementEmployee = movementEmployee;
    }

    public String getParexelGitlab() {
        return parexelGitlab;
    }

    public void setParexelGitlab(String parexelGitlab) {
        this.parexelGitlab = parexelGitlab;
    }

    public String getIbmGitlab() {
        return ibmGitlab;
    }

    public void setIbmGitlab(String ibmGitlab) {
        this.ibmGitlab = ibmGitlab;
    }

    public String getRallyName() {
        return rallyName;
    }

    public void setRallyName(String rallyName) {
        this.rallyName = rallyName;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }
}
