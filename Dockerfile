FROM openjdk:8-jdk-alpine

RUN apk update
RUN apk add maven
RUN apk add bash

WORKDIR /workdir

ADD pom.xml /workdir/pom.xml
ADD startup.sh /workdir/startup.sh
RUN ["chmod", "+x", "/workdir/startup.sh"]

ADD src /workdir/src
RUN ["mvn", "package"]

ENTRYPOINT ["./startup.sh"]




