#!/bin/bash
apiKeys=""
year=""
connectionTimeout=""
statisticsTable=""
identityTable=""
dbBase=""
dbServer=""
dbUsername=""
dbPassword=""
properties=""
while [ -n "$1" ]
do
	case "$1" in 
		-apiKeys) apiKeys="$2"
		shift
	;;
	    -properties) properties="$2"
		shift
	;;
		-year) year="$2"
		shift
	;;
		-connectionTimeout) connectionTimeout="$2"
		shift
	;;
		-statisticsTable) statisticsTable="$2"
		shift
	;;
		-identityTable) identityTable="$2"
		shift
	;;
		-dbBase) dbBase="$2"
		shift
	;;
		-dbServer) dbServer="$2"
		shift
	;;
		-dbUsername) dbUsername="$2"
		shift
	;;
		-dbPassword) dbPassword="$2"
		shift
	;;
	esac
shift
done

comToRun=""
if [ -n "${apiKeys}" ]; then
  comToRun="$comToRun -DapiKeys=$apiKeys" 
fi

if [ -n "${year}" ]; then
  comToRun="$comToRun -Dyear=$year" 
fi

if [ -n "${connectionTimeout}" ]; then
  comToRun="$comToRun -DconnectionTimeout=$connectionTimeout" 
fi

if [ -n "${statisticsTable}" ]; then
  comToRun="$comToRun -DstatisticsTable=$statisticsTable" 
fi

if [ -n "${identityTable}" ]; then
  comToRun="$comToRun -DidentityTable=$identityTable" 
fi

if [ -n "${dbBase}" ]; then
  comToRun="$comToRun -DdbBase=$dbBase" 
fi

if [ -n "${dbServer}" ]; then
  comToRun="$comToRun -DdbServer=$dbServer" 
fi

if [ -n "${dbUsername}" ]; then
  comToRun="$comToRun -DdbUsername=$dbUsername" 
fi

if [ -n "${dbPassword}" ]; then
  comToRun="$comToRun -DdbPassword=$dbPassword" 
fi

if [ -n "${properties}" ]; then
  comToRun="$comToRun -DdbPassword=$properties"
fi
echo "/usr/lib/jvm/java-8-openjdk-amd64/bin/java -jar $comToRun target/statisticsApp-0.0.1-SNAPSHOT.jar"
eval "java -jar $comToRun target/statisticsApp-0.0.1-SNAPSHOT.jar"



